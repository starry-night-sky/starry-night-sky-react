import { Route, Routes } from "react-router-dom";
import AuthStateAccessor from "./middleware/AuthStateAccessor";
import HomePage from "./pages/HomePage";

function App() {
  return (
    <div className="App">
      <Routes>
        <Route
          path="/"
          element={
            <AuthStateAccessor>
              <HomePage />
            </AuthStateAccessor>
          }
        />
      </Routes>
    </div>
  );
}

export default App;
