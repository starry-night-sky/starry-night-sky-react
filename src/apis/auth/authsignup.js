import axios from "axios";
import { getApiDefaults } from "../base";

export const ApiAuthSignup = (input, callback, errorCallback) => {
  var domain = getApiDefaults().domain;
  ApiAuthSignupRequestValidation(
    input,
    () => {
      axios({
        method: "post",
        url: `${domain}/auth/signup`,
        data: {
          username: input.username,
          password: input.password,
          avatar: input.avatar,
        },
      })
        .then((response) => {
          if (response.errorMessage) {
            errorCallback(response.errorMessage);
          } else {
            callback(response);
          }
        })
        .catch(() => {
          errorCallback("ApiAuthSignup Failed!");
        });
    },
    (error) => {
      errorCallback(error);
    }
  );
};

const ApiAuthSignupRequestValidation = (input, callback, errorCallback) => {
  if (!input.username) {
    errorCallback("Username is null or empty");
    return;
  }

  if (!new RegExp("^[a-zA-Z0-9]{6,30}$").test(input.username)) {
    errorCallback(
      "Username is must be alphanumeric and be at least 6-30 characters"
    );
    return;
  }

  if (!input.password) {
    errorCallback("Password is null or empty");
    return;
  }

  if (
    !new RegExp("^(?=.*[A-Z])(?=.*[a-z])(?=.*d)(?=.*[^A-Za-z0-9]).{8,}$").test(
      input.password
    )
  ) {
    errorCallback(
      "Password must have at least 8 characters in total, must contain at least one uppercase letter, must contain at least one lowercase letter, must contain at least one digit, and must contain at least one non-alphanumeric character."
    );
    return;
  }

  if (!input.avatar) {
    errorCallback("Avatar is required!");
    return;
  }

  callback();
};
