import axios from "axios";
import { getApiDefaults } from "../base";

export const ApiProfileGet = (input, callback, errorCallback) => {
  var domain = getApiDefaults().domain;
  ApiProfileGetRequestValidation(
    input,
    () => {
      axios({
        method: "post",
        url: `${domain}/profile/get`,
        headers: {
          "X-Access-Token": input.accessToken,
        },
        data: {
          username: input.username,
        },
      })
        .then((response) => {
          if (response.errorMessage) {
            errorCallback(response.errorMessage);
          } else {
            callback(response);
          }
        })
        .catch(() => {
          errorCallback("ApiProfileGet Failed!");
        });
    },
    (error) => {
      errorCallback(error);
    }
  );
};

const ApiProfileGetRequestValidation = (input, callback, errorCallback) => {
  if (!input.username) {
    errorCallback("Username is null or empty");
    return;
  }

  if (!new RegExp("^[a-zA-Z0-9]{6,30}$").test(input.username)) {
    errorCallback(
      "Username is must be alphanumeric and be at least 6-30 characters"
    );
    return;
  }

  callback();
};
