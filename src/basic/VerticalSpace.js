import React from "react";

const VerticalSpace = ({ size }) => {
  return <div style={{ height: size }} />;
};

export default VerticalSpace;
