import {
  Button,
  HStack,
  Heading,
  Input,
  Tab,
  TabList,
  TabPanel,
  TabPanels,
  Tabs,
  VStack,
  useToast,
} from "@chakra-ui/react";
import React, { useState } from "react";
import VerticalSpace from "../basic/VerticalSpace";
import { ApiAuthSignup } from "../apis/auth/authsignup";
import { ApiAuthLogin } from "../apis/auth/authlogin";
import { ApiProfileGet } from "../apis/profile/profileget";

const Login = ({ setAuthFn }) => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [avatar, setAvatar] = useState("");
  const [signupMode, setSignupMode] = useState(false);
  const toast = useToast();

  const resetFormFn = () => {
    setUsername("");
    setPassword("");
    setAvatar("");
  };

  const toastSignupSuccess = () => {
    resetFormFn();
    toast({
      title: "Signup Success",
      description: "Your account has been created!",
      status: "success",
      duration: 5000,
      isClosable: true,
      position: "bottom-left",
    });
    setSignupMode(false);
  };

  const signupApiCall = () => {
    ApiAuthSignup(
      {
        username,
        password,
        avatar,
      },
      () => toastSignupSuccess(),
      toastSignupError
    );
  };

  const toastSignupError = (errorMessage) => {
    toast({
      title: "Signup Request Invalid",
      description: errorMessage,
      status: "error",
      duration: 60000,
      isClosable: true,
      position: "bottom-left",
    });
  };

  const toastLoginError = (errorMessage) => {
    toast({
      title: "Login Request Invalid",
      description: errorMessage,
      status: "error",
      duration: 30000,
      isClosable: true,
      position: "bottom-left",
    });
  };

  const loginApiCall = () => {
    ApiAuthLogin(
      {
        username,
        password,
      },
      (response) => {
        console.log(response.data);
        ApiProfileGet(
          {
            username,
            accessToken: response.data.data.accessToken,
          },
          (profileResponse) => {
            console.log(profileResponse.data);
            setAuthFn({
              token: response.data.data.accessToken,
              username,
              avatar: profileResponse.data.data.avatar,
            });
            resetFormFn();
          },
          (error) => {
            resetFormFn();
            toastLoginError(error);
          }
        );
      },
      toastLoginError
    );
  };

  return (
    <VStack w="300px">
      <HStack>
        <Heading as="h3" size="3xl" marginBottom="24px">
          🌃
        </Heading>
      </HStack>
      <HStack>
        <Heading as="h3" size="lg" marginBottom="16px">
          StarryNightSky
        </Heading>
      </HStack>
      <Tabs
        isFitted={true}
        colorScheme="purple"
        w="280px"
        index={signupMode ? 1 : 0}
        onChange={(index) => setSignupMode(index === 1)}
      >
        <TabList>
          <Tab>Login</Tab>
          <Tab>Signup</Tab>
        </TabList>
        <TabPanels>
          <TabPanel align="center">
            <VerticalSpace size={24} />
            <Input
              placeholder="Username"
              onChange={(e) => setUsername(e.target.value)}
            />
            <VerticalSpace size={16} />
            <Input
              placeholder="Password"
              type="password"
              onChange={(e) => setPassword(e.target.value)}
            />
            <VerticalSpace size={24} />
            <Button colorScheme="purple" w="240px" onClick={loginApiCall}>
              Login
            </Button>
          </TabPanel>
          <TabPanel align="center">
            <VerticalSpace size={24} />
            <Input
              placeholder="Username"
              value={username}
              onChange={(e) => setUsername(e.target.value)}
            />
            <VerticalSpace size={16} />
            <Input
              placeholder="Avatar"
              value={avatar}
              onChange={(e) => setAvatar(e.target.value)}
            />
            <VerticalSpace size={16} />
            <Input
              placeholder="Password"
              value={password}
              type="password"
              onChange={(e) => setPassword(e.target.value)}
            />
            <VerticalSpace size={24} />
            <Button colorScheme="purple" w="240px" onClick={signupApiCall}>
              Signup
            </Button>
          </TabPanel>
        </TabPanels>
      </Tabs>
    </VStack>
  );
};

export default Login;
