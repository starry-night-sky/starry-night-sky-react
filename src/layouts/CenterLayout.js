import { Center } from "@chakra-ui/react";
import React from "react";

const CenterLayout = ({ children }) => {
  return (
    <Center h="100vh" w="100vw" backgroundColor="black">
      {children}
    </Center>
  );
};

export default CenterLayout;
