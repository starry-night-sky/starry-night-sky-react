import React from "react";
import { useRecoilState } from "recoil";
import { authState } from "../state/auth";
import LoginPage from "../pages/LoginPage";

const AuthStateAccessor = ({ children }) => {
  const [authentication, setAuthentication] = useRecoilState(authState);
  const setAuthenticationData = (data) => {
    setAuthentication(data);
  };
  return (
    <>
      {!authentication.token ? (
        <LoginPage setAuthFn={setAuthenticationData} />
      ) : (
        <>{children}</>
      )}
    </>
  );
};

export default AuthStateAccessor;
