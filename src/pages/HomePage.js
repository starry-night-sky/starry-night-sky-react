import {
  Avatar,
  Breadcrumb,
  BreadcrumbItem,
  BreadcrumbLink,
  Flex,
  Heading,
  VStack,
} from "@chakra-ui/react";
import React from "react";
import VerticalSpace from "../basic/VerticalSpace";

const HomePage = () => {
  return (
    <Flex direction="column" backgroundColor="black">
      <Flex
        h="70px"
        justifyContent="space-between"
        alignItems="center"
        backgroundColor="#7149C6"
        padding="0px 20px"
      >
        <Heading as="h4" size="md">
          StarryNightSky
        </Heading>
        <Avatar
          size="sm"
          name="Dan Abrahmov"
          src="https://bit.ly/dan-abramov"
        />
      </Flex>
      <VStack h="50px" backgroundColor="#0C134F">
        <Breadcrumb marginTop="12px" marginBottom="16px">
          <BreadcrumbItem>
            <BreadcrumbLink color="#D4ADFC">Home</BreadcrumbLink>
          </BreadcrumbItem>
        </Breadcrumb>
      </VStack>
      <VStack h="calc(100vh - 120px)" overflow="auto" padding="20px">
        <p>test</p>
        <VerticalSpace size={12} />
        <p>test</p>
        <VerticalSpace size={12} />
        <p>test</p>
        <VerticalSpace size={12} />
        <p>test</p>
        <VerticalSpace size={12} />
        <p>test</p>
        <VerticalSpace size={12} />
        <p>test</p>
        <VerticalSpace size={12} />
        <p>test</p>
        <VerticalSpace size={12} />
        <p>test</p>
        <VerticalSpace size={12} />
        <p>test</p>
        <VerticalSpace size={12} />
        <p>test</p>
        <VerticalSpace size={12} />
        <p>test</p>
        <VerticalSpace size={12} />
        <p>test</p>
        <VerticalSpace size={12} />
        <p>test</p>
        <VerticalSpace size={12} />
        <p>test</p>
        <VerticalSpace size={12} />
        <p>test</p>
        <VerticalSpace size={12} />
        <p>test</p>
        <VerticalSpace size={12} />
        <p>test</p>
        <VerticalSpace size={12} />
        <p>test</p>
        <VerticalSpace size={12} />
        <p>test</p>
        <VerticalSpace size={12} />
        <p>test</p>
        <VerticalSpace size={12} />
        <p>test</p>
        <VerticalSpace size={12} />
        <p>test</p>
        <VerticalSpace size={12} />
        <p>test</p>
      </VStack>
    </Flex>
  );
};

export default HomePage;
