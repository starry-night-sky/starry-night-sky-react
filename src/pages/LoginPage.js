import React from "react";
import CenterLayout from "../layouts/CenterLayout";
import Login from "../components/Login";

const LoginPage = ({ setAuthFn }) => {
  return (
    <CenterLayout>
      <Login setAuthFn={setAuthFn} />
    </CenterLayout>
  );
};

export default LoginPage;
